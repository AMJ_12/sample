from django.db import models



class userreg(models.Model):
    name=models.CharField(max_length=25)
    email=models.EmailField(max_length=50)
    userid=models.CharField(max_length=8)
    userpassword=models.CharField(max_length=8)

class adminreg(models.Model):
    email=models.EmailField(max_length=50)
    password=models.CharField(max_length=8)    

class comments(models.Model):
    comment=models.CharField(max_length=100)
    userid=models.CharField(max_length=8)

class keyword(models.Model):
    reported=models.CharField(max_length=100)

class blocked(models.Model):
    keywords=models.CharField(max_length=100)
    userid=models.CharField(max_length=8)


def __str1__(self):
        return self.userreg
def __str2__(self):
        return self.adminreg
def __str3__(self):
        return self.comments
def __str4__(self):
        return self.keyword
def __str5__(self):
        return self.blocked

# Create your models here.
