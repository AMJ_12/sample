from django.contrib import admin
from .models import userreg
from .models import adminreg
from .models import comments
from .models import keyword
from .models import blocked


admin.site.register(userreg)
admin.site.register(adminreg)
admin.site.register(comments)
admin.site.register(keyword)
admin.site.register(blocked)



class UserRegAdmin(admin.ModelAdmin):
    list_display=['Name','Email','Mobile','Userid','Password']

class AdminRegAdmin(admin.ModelAdmin):
    list_display=['Email','Password']
    
class CommentsAdmin(admin.ModelAdmin):
    list_display=['Comment','Userid']

class KeywordAdmin(admin.ModelAdmin):
    list_display=['Reported']

class BlockedAdmin(admin.ModelAdmin):
    list_display=['Keywords','Userid']


# Register your models here.
